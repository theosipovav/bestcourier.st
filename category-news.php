<?php get_header(); ?>
<div class="main_title">
	<div class="container">
		<div class="row">
			
							<h1><?php single_cat_title(); ?></h1>

				<div id="dimox_breadcrumbs">
				<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
				</div>
			
		</div>
	</div>
</div>
<div class="container">
		<div class="row">
			<div class="col-sm-8 news_list">
				<?php if (have_posts()) : 
						while (have_posts()) : the_post();  ?>
							<div class="row news_item">
							
								<div class="col-sm-4 mg_b_30">
									<a href="<?php the_permalink() ?>"><?php echo get_the_post_thumbnail( $page->ID, array(240,240)); ?></a> 
								</div>
								<div class="col-sm-8 mg_b_30">	
									<span class="entry-info"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_time(' j F Y'); ?></span>
									<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
									<div itemprop="articleBody">
										<?php the_excerpt(); ?>
									</div>
									<a class="btn btn-primary" href="<?php the_permalink() ?>" rel="bookmark">Далее</a>
							
								
								</div>
						
							</div>
						<?php endwhile; ?>
					<?php else : ?>
						<?php include(TEMPLATEPATH . "/404.php"); ?>
					<?php endif; ?>
				<?php wp_pagenavi(); ?>
			</div>
			<div class="col-sm-4">
				<div class="menu_sidebar">
					<div class="menu_sidebar--title">
						Услуги
					</div>
					<?php wp_nav_menu( [ 
							'container' => 'false',
							'container_id' => '',
							'container_class' => '',
							'theme_location'  => 'sidebar-menu-u'
						] ); ?>
				</div>
				<div class="menu_sidebar">
					<div class="menu_sidebar--title">
						Производство
					</div>
					<?php wp_nav_menu( [ 
							'container' => 'false',
							'container_id' => '',
							'container_class' => '',
							'theme_location'  => 'sidebar-menu-p'
						] ); ?>
				</div>
			</div>			
		</div>
			
</div>
	
<?php get_footer(); ?>