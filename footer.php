<div class="grid_section--5dd540bf9d70a484130270">
	<div class="container">
		<div class="g_row">
			<div class="grid_col--5dd540bfa1893897712054">
				<div class="grid_col_wrp">
					<div class="html_60 section_social_btn " data-uniqid="m_html--5dd540fea3476528479225" data-version="v.0002">
						<div class="title_wrap_top"> </div>
						<div class="cont_module_wrp">
							<div class="media-icon">
								<div class="text-center">
									<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter"></div>
								</div>
							</div>

						</div>
					</div>
					<div class="html_61  " data-uniqid="m_html--5dd548a0caea4312072307" data-version="v.0002">
						<div class="title_wrap_top"> </div>
						<div class="cont_module_wrp">
							<div class="copyright">2003 - 2020 @ Компания BESTCOURIER</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script src="<?php bloginfo('template_directory'); ?>/includes/mmenu/jquery.mmenu.min.all.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.mobilemenu.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.fias.min.js" charset="UTF-8"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>
<?php wp_footer(); ?>
<script type="text/javascript">
	document.addEventListener('wpcf7mailsent', function sendMail(event) {
		if ('5' == event.detail.contactFormId) {
			ym(22877764, 'reachGoal', 'courier-success');
			return true;
		}
		if ('190' == event.detail.contactFormId) {
			ym(22877764, 'reachGoal', 'call-back');
			return true;
		}
		if ('264' == event.detail.contactFormId) {
			ym(22877764, 'reachGoal', 'order-success');
			return true;
		}
	}, false);
</script>



<?php if (!in_array(intval(get_the_ID()), [63, 65, 146])) : ?>
	<div class="call-courier">
		<button class="popmake-vyzov-kurera-plavayushhaya-knopka call-courier-btn">
			<div class="call-courier-icon">
				<i class="fas fa-car-side call-courier-btn--icon call-courier-btn--icon-after"></i>
				<i class="fas fa-user call-courier-btn--icon call-courier-btn--icon-before"></i>
			</div>
			<span class="call-courier-btn--text">Вызвать<br>курьера</span>
		</button>
	</div>
<?php endif ?>

</body>

</html>