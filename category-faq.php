<?php get_header(); ?>
<div class="main_title">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<h1 class="main_h1"><?php single_cat_title(); ?></h1>
				<div id="dimox_breadcrumbs">
						<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-8 faq_wrapper">
				<?php $num_post = 1;  ?>
				
				
				
				<?php /*query_posts($query_string.'&posts_per_page=1000'); */ ?>
				<?php if (have_posts()) : while (have_posts()) : the_post();  ?>
				
				
				
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading<?php echo $num_post;  ?>">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $num_post;  ?>" aria-expanded="true" aria-controls="collapse<?php echo $num_post;  ?>">
										<?php the_title(); ?>
									</a>
								</h4>
							</div>
							<div id="collapse<?php echo $num_post;  ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $num_post;  ?>">
								<div class="panel-body">
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					</div>			
			
			

				
					<?php $num_post++;  ?>
				<?php endwhile; ?>
				<?php else : ?>
				<?php /*include(TEMPLATEPATH . "/404.php");*/ ?>
				<?php endif; ?>
				
		</div>
		<div class="col-sm-4">
			<div class="menu_sidebar">
				<div class="menu_sidebar--title">
					Услуги
				</div>
				<?php wp_nav_menu( [ 
						'container' => 'false',
						'container_id' => '',
						'container_class' => '',
						'theme_location'  => 'sidebar-menu-u'
					] ); ?>
			</div>
			<div class="menu_sidebar">
				<div class="menu_sidebar--title">
					Производство
				</div>
				<?php wp_nav_menu( [ 
						'container' => 'false',
						'container_id' => '',
						'container_class' => '',
						'theme_location'  => 'sidebar-menu-p'
					] ); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>