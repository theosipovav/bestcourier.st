<?php
/*
Template Name: Калькулятор
Template Post Type: page
*/
?>
<?php
/*
$tarifi = array(
	"6d" => array(
		200 => 70,
		500 => 90,
		1000 => 115
	),
	"4d" => array(
		200 => 90,
		500 => 100,
		1000 => 130
	),
	"3d" => array(
		200 => 100,
		500 => 115,
		1000 => 140
	),
	"1d" => array(
		200 => 170,
		500 => 190,
		1000 => 210
	),
	"4h" => array(
		1000 => 360
	),
	"2h" => array(
		1000 => 500
	)
);
*/

?>
<?php get_header(); ?>
<div class="main_title">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php if (have_posts()) :
					while (have_posts()) : the_post();  ?>
						<h1><?php the_title(); ?></h1>
					<?php endwhile; ?>
				<?php endif; ?>
				<div id="dimox_breadcrumbs">
					<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if (1 == 2) { ?>
	<?php foreach ($table as $k => $v) { ?>
		<table class="table table-bordered table-hover">
			<caption>Срок доставки: <?php echo $v["caption"]; ?></caption>
			<thead>
				<tr>
					<th>Вес отправления</th>
					<th>Тариф, руб</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($v["body"] as $val) { ?>
					<tr>
						<td>до <?php echo $val[0]["c"]; ?> гр</td>
						<td><?php echo $val[1]["c"]; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	<?php } ?>
<?php } ?>
<div class="container">
	<div class="row">
		<div class="col-lg-offset-2 col-lg-8">

			<?php include("includes/calc.php"); ?>

			<div class="calc_text_info mg_b_20">
				<p>Данный сервис поможет Вам узнать ориентировочную стоимость доставки Ваших отправлений</p>
			</div>
			<div class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-3 control-label">Количество отправлений</label>
					<div class="col-sm-9">
						<input type="text" name="count_send" class="form-control" placeholder="Укажите количество отправлений">
					</div>
				</div>
				<div class="form-group" style="display: none;">
					<label class="col-sm-3 control-label">Вес, гр</label>
					<div class="col-sm-9">
						<input type="text" name="weight" class="form-control" value="100" placeholder="Укажите вес отправления в граммах">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Срок доставки</label>
					<div class="col-sm-9">
						<select name="period" class="form-control">
							<option value="">Выберите срок доставки</option>
							<?php foreach ($table as $k => $n) { ?>
								<?php if ($k == '6d') : ?>
									<option value="<?php echo $k; ?>" selected><?php echo $n["caption"]; ?></option>
								<?php else : ?>
									<option value="<?php echo $k; ?>"><?php echo $n["caption"]; ?></option>
								<?php endif ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<div id="if_zabor" class="if_zabor form-group" style="display: block;">
					<label class="col-sm-3 control-label">Нужно ли забрать отправления?</label>
					<div class="col-sm-9">
						<div class="radio"><label><input type="radio" name="take" value="1" checked> Да, нужно</label></div>
						<div class="radio"><label><input type="radio" name="take" value="0"> Нет, привезем сами</label></div>

					</div>
				</div>


				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<div id="if_error" class="d-none calc_text_danger">Заполните все поля формы рассчета стоимости</div>
						<button type="submit4" id="calc_btn_calculate" class="btn btn-success">Рассчитать</button>
						<div id="calc_result" class="calc_result h3"></div>
						<span id="courier_order" class="btn btn-accent mg_t_15 d-none popmake-vyzvat-kurera" style="cursor: pointer;">Вызвать курьера</span>
					</div>
				</div>
				<div id="if_day" class="d-none calc_text_info mg_y_20">
					<p>НДС не облагается.</p>
					<p>Расчет выполнен для доставки в пределах МКАД. Для расчета стоимости доставки за пределами МКАД перейдите в раздел «ТАРИФЫ по Подмосковью» или обратитесь к нашим менеджерам по тел. 495 127 27 68</p>
					<p>День приема отправлений не считается днем доставки.</p>
					<p>Расчет является предварительным и не учитывает стоимость возможных дополнительных услуг. Точную стоимость заказа, а также размер ПЕРСОНАЛЬНОЙ СКИДКИ Вы можете узнать, обратившись к нашим менеджерам по тел. 495 127 27 68.</p>
				</div>
				<div id="if_hour" class="d-none calc_text_info mg_y_20">
					<p>НДС не облагается.</p>
					<p>Расчет выполнен для доставки в пределах МКАД. Для расчета стоимости доставки за пределами МКАД или если адрес отправления или адрес доставки является труднодоступным (сложная транспортная доступность, требующая либо увеличенного времени на доставку, либо дополнительные транспортные затраты), перейдите в раздел «ТАРИФЫ Срочная доставка» или обратитесь к нашим менеджерам по тел. 495 127 27 68 доб. 110, 211.</p>
					<p>Ожидание курьера в офисе заказчика или по адресу получателя первые 10 минут бесплатно, каждые следующие полные или неполные 20 минут оплачиваются по тарифу 118 руб.</p>
				</div>

			</div>

		</div>
	</div>
</div>
<?php get_footer(); ?>