<?php
/*
Template Name: Услуги
Template Post Type: page
*/
?>
<?php get_header(); ?>
<div class="main_title">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php if (have_posts()) : 
						while (have_posts()) : the_post();  ?>
							<h1><?php the_title(); ?></h1>
					<?php endwhile; ?>
				<?php endif; ?>	
				<div id="dimox_breadcrumbs">
				<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<?php if (have_posts()) : 
				while (have_posts()) : the_post();  ?>
					<div class="page-post section_service">
					<?php
												wp_nav_menu( array(
													'theme_location' => 'uslugi-menu',
													'walker'         => new My_Walker_Nav_Menu(),
													'menu'            => '', 
													'container'       => 'false', 
													'container_class' => 'row', 
													'container_id'    => 'cont_module_wrp',
													'menu_class'      => 'menu', 
													'menu_id'         => '',
													'echo'            => true,
													'fallback_cb'     => 'wp_page_menu',
													'before'          => '',
													'after'           => '',
													'link_before'     => '',
													'link_after'      => '',
													'items_wrap'      => '<div class="cont_module_wrp" id="cont_module_wrp"><div class="row">%3$s</div></div>',
													'depth'           => 0,
												) );							
											?>
					
							<?php the_content(); ?>
						</div>	
					
				<?php endwhile; ?>
			<?php else : ?>
				<?php include(TEMPLATEPATH . "/404.php"); ?>
			<?php endif; ?>	
		</div>
	</div>
</div>
<?php get_footer(); ?>