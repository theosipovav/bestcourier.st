<?php
/*
Template Name: Главная страница
Template Post Type: page
*/
?>
<?php
get_header();
?>
<div class="grid_row--5dbaee39b6041852128154 hidden-xs">
	<div class="g_row">
		<div class="grid_col--5dbaee39b6872c98023419">
			<div class="grid_col_wrp">
				<div class="html_52 section_slider " data-uniqid="m_html--5dd434fb77ee0823963147" data-version="v.0002">
					<div class="title_wrap_top"> </div>
					<div class="cont_module_wrp">
						<div class="owl-carousel owl-theme">

							<div class="item">
								<img src="<?php bloginfo('template_directory'); ?>/images/slider/1.jpg">
								<div class="slider_caption">
									<div class="container">
										<div class="row">
											<div class="col-sm-12">
												<div class="slider_title">Курьерская служба</div>
												<div class="slider_descr">Несрочная доставка отправлений
													<br>от 70 руб. </div><a href="/tarify/nesrochnaya-dostavka/"><span class="slider_btn">Узнать подробнее</span></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<img src="<?php bloginfo('template_directory'); ?>/images/slider/4.jpg">
								<div class="slider_caption">
									<div class="container">
										<div class="row">
											<div class="col-sm-12">
												<div class="slider_title">Курьерская служба</div>
												<div class="slider_descr"> Экспресс доставка документов
													<br> день в день по Москве и МО от 330 руб. </div> <span class="slider_btn popmake-265">Оставить заявку</span>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="item ">
								<img class="slider--img" src="<?php bloginfo('template_directory'); ?>/images/slider/slider-shares.jpg">
								<div class="slider_caption">
									<div class="container">
										<div class="row">
											<div class="col-sm-12">
												<div class="slider_title"><?= get_field("home-baner-text"); ?></div>
												<div class="slider_descr"><?= get_field("home-baner-desc") ?></div> <span class="slider_btn popmake-265">Оставить заявку</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<script>
						$('.owl-carousel').owlCarousel({
							items: 1,
							autoHeight: true,
							loop: true,
							autoplay: true,
							animateOut: 'fadeOut',
							autoplayTimeout: 2800,
							autoplayHoverPause: false
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
// Секция "Популярные услуги"
if (false) include "content-section-popular_services.php";
?>


<?php if (false) : ?>
	<?php
	// update 2020-10-24
	// Информацию об Акции -10% добавить в банер на главной странице. Заменить фоновую картинку (подберем в процессе, чуть позже).
	?>
	<?php if (get_field("home-baner-status")) { ?>
		<div class="home_banner" style="cursor:pointer;background:url(<?php echo get_field("home-baner"); ?>) no-repeat" onClick="location.href='/aktsii/'">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="hb_title"><?php echo get_field("home-baner-text"); ?></div>
						<div class="hb_description"><?php echo get_field("home-baner-desc"); ?></div>
					</div>
				</div>
			</div>
		</div>
	<?php }	?>
<?php endif ?>
<?php if (false) : ?>
	<?php
	// 2020-10-24
	// Блок “Наши услуги” поднять наверх, первым в теле главной страницы, сразу после оранжевой строчки меню.
	?>
	<div class="grid_row--5dbaf06c7b22d058038119 ">
		<div class="container">
			<div class="g_row">
				<div class="grid_col--5dbaf06c7b645c97405230">
					<div class="grid_col_wrp">
						<div class="html_47 section_service m_title_10" data-uniqid="m_html--5dd41b25aba87732105026" data-version="v.0002">
							<div class="title_wrap_top">
								<div class="title">Наши <span>услуги</span></div>
								<div class="post_title">Наши <span>услуги</span></div>
							</div>
							<?php
							wp_nav_menu(array(
								'theme_location' => 'uslugi-menu',
								'walker'         => new My_Walker_Nav_Menu(),
								'menu'            => '',
								'container'       => 'false',
								'container_class' => 'row',
								'container_id'    => 'cont_module_wrp',
								'menu_class'      => 'menu',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<div class="cont_module_wrp" id="cont_module_wrp"><div class="row">%3$s</div></div>',
								'depth'           => 0,
							));
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>





<?php
// Секция "Вызов курьера"
if (false) include "content-section-courier_call.php";
?>









<div class="grid_section--5dd52c06f1eb8462121494 ">
	<div class="container">
		<div class="g_row">
			<div class="grid_col--5dd52c06f4bc6c41261312">
				<div class="grid_col_wrp">
					<div class="html_55 best_service m_title_10" data-uniqid="m_html--5dd52925388d1274444908" data-version="v.0002">
						<div class="title_wrap_top">
							<div class="title">Наши <span>преимущества</span></div>
							<div class="post_title">Наши <span>преимущества</span></div>
							<div class="desc_top">
								<p>Москва знаменита своим невероятно интенсивным уровнем жизни. В таком мегаполисе дорога каждая минута, особенно если учитывать тот факт, что для любого успешного бизнесмена наиболее важным ресурсом является время. Поэтому служба доставки по Москве актуальна как никогда. Наши услуги отличаются высоким качеством и оперативностью. Именно это позволяет нам выдерживать жесточайшую конкуренцию. Поэтому вы сможете полностью на нас положиться.</p>
							</div>
						</div>
						<div class="cont_module_wrp">
							<div class="row">
								<div class="col-md-6">
									<div class="tiaser">
										<div class="tiaser-title">Надежные курьеры</div>
										<div class="tiaser-desc">Все курьеры - жители Москвы, знающие свой город</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tiaser">
										<div class="tiaser-title">Опытные менеджеры</div>
										<div class="tiaser-desc">Персональные менеджеры, сопровождающие выполнение вашего заказа</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tiaser">
										<div class="tiaser-title">Клиентоориентированность</div>
										<div class="tiaser-desc">Индивидуальный подход к каждому клиенту</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tiaser">
										<div class="tiaser-title">Много лет на рынке</div>
										<div class="tiaser-desc">Мы - стабильная компания, проверенная временем</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tiaser">
										<div class="tiaser-title">Ценовая политика</div>
										<div class="tiaser-desc">Оптимальное соотношение цены и качества услуг</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="tiaser">
										<div class="tiaser-title">Высокая надежность</div>
										<div class="tiaser-desc">Обеспечиваем максимальный уровень конфиденциальности</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="grid-row-partners">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="grid_col_wrp">
					<div id="SectionPartners" class="html_529 section_partners m_title_10">
						<div class="title_wrap_top">
							<div class="title">Наши <span>клиенты</span></div>
							<div class="post_title">Наши <span>клиенты</span></div>
						</div>
						<div class="cont_module_wrp">
							<div class="owl-partners owl-carousel owl-theme">
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/partners/1.png"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/partners/2.png"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/partners/3.png"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/partners/4.png"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/partners/5.png"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/partners/6.png"></div>
							</div>
						</div>
						<script>
							$('.owl-partners').owlCarousel({
								responsive: {
									0: {
										items: 2
									},
									480: {
										items: 3
									},
									768: {
										items: 4
									},
									1024: {
										items: 5
									}
								},
								responsiveClass: true,
								margin: 40,
								autoHeight: true,
								loop: true
							});
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
// Секция "ТРИ ПРИЧИНЫ ВЫБРAТЬ BESTCOURIER ТРИ ПРИЧИНЫ ВЫБРAТЬ BESTC"
if (false) include "content-section-why_we.php";
?>

<div class="grid_row--5dbeea494872b012763554 ">
	<div class="container">
		<div class="g_row">
			<div class="grid_col--5dbeea4948b43861535786">
				<div class="grid_col_wrp">
					<div class="html_57 section_number_about_us m_title_10" data-uniqid="m_html--5dd53bf55ad6a677810927" data-version="v.0002">
						<div class="title_wrap_top">
							<div class="title">BestCourier <span>это</span></div>
							<div class="post_title">BestCourier <span>это</span></div>
						</div>
						<div class="cont_module_wrp">
							<div class="row">
								<div class="col-sm-6 col-md-3 text-center">
									<div class="num">150</div>
									<div class="num_desc">курьеров</div>
								</div>
								<div class="col-sm-6 col-md-3 text-center">
									<div class="num">9000</div>
									<div class="num_desc">доставок ежедневно</div>
								</div>
								<div class="col-sm-6 col-md-3 text-center">
									<div class="num">18 лет</div>
									<div class="num_desc">на рынке</div>
								</div>
								<div class="col-sm-6 col-md-3 text-center">
									<div class="num">400</div>
									<div class="num_desc">постоянных клиентов</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
// Секция "КОРОТКО О НАС"
if (false) include "content-section-about.php";
?>


<div class="grid-section--5dd53e4c36041862534429 ">
	<div class="container">
		<div class="g_row">
			<div class="grid_col-5dd53e4c39999817050256">
				<div class="grid_col_wrp">
					<div class="html_59 section_review m_title_10" data-uniqid="m_html--5dd53ecfd2d83304622508" data-version="v.0002">
						<div class="title_wrap_top">
							<div class="title">Отзывы <span>о нас</span></div>
							<div class="post_title">Отзывы <span>о нас</span></div>
						</div>
						<div class="cont_module_wrp">
							<div class="owl-reviews owl-carousel owl-theme">
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/review/small/0.jpg" data-full_src="<?php bloginfo('template_directory'); ?>/images/review/0.jpg"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/review/small/1.jpg" data-full_src="<?php bloginfo('template_directory'); ?>/images/review/1.jpg"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/review/small/2.jpg" data-full_src="<?php bloginfo('template_directory'); ?>/images/review/2.jpg"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/review/small/3.jpg" data-full_src="<?php bloginfo('template_directory'); ?>/images/review/3.jpg"></div>
								<div class="item"><img src="<?php bloginfo('template_directory'); ?>/images/review/small/4.jpg" data-full_src="<?php bloginfo('template_directory'); ?>/images/review/4.jpg"></div>
							</div>

							<script>
								$('.owl-reviews').owlCarousel({
									responsive: {
										0: {
											items: 1
										},
										480: {
											items: 2
										},
										768: {
											items: 3
										},
										1024: {
											items: 4
										}
									},
									responsiveClass: true,
									margin: 40,
									autoHeight: true,
									loop: false
								});
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div id="common-home" class="container">
	<div class="row">
		<div id="content" class="col-sm-12"></div>
	</div>
</div>
<div id="grid_bottom"> </div>
</div>
<?php
get_footer();
?>