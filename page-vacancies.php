<?php
/*
Template Name: Вакансии
Template Post Type: page
*/
?>

<?php get_header(); ?>
<div class="main_title">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <?php if (have_posts()) :
                    while (have_posts()) : the_post();  ?>
                        <h1><?php the_title(); ?></h1>
                    <?php endwhile; ?>
                <?php endif; ?>
                <div id="dimox_breadcrumbs">
                    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if (have_posts()) :
                while (have_posts()) : the_post();  ?>
                    <div class="page-post">
                        <?php the_content(); ?>
                    </div>

                <?php endwhile; ?>
            <?php else : ?>
                <?php include(TEMPLATEPATH . "/404.php"); ?>
            <?php endif; ?>
        </div>

    </div>
</div>
<?php get_footer(); ?>