<?php
/*
Template Name: Контакты
Template Post Type: page
*/
?>
<?php get_header(); ?>
<div class="main_title">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php if (have_posts()) : 
						while (have_posts()) : the_post();  ?>
							<h1><?php the_title(); ?></h1>
					<?php endwhile; ?>
				<?php endif; ?>	
				<div id="dimox_breadcrumbs">
				<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
				<?php if (have_posts()) : 
					while (have_posts()) : the_post();  ?>
						<div class="page-post">	
								<?php the_content(); ?>
							</div>	
						
					<?php endwhile; ?>
				<?php else : ?>
					<?php include(TEMPLATEPATH . "/404.php"); ?>
				<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<p><strong>Адрес: улица Новоостаповская, дом 5, строение 1</strong></p>
			<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=lJcC4RfpcfEZxZxWenPxXuINm3hZj7Mp&width=100%&height=450"></script>
		</div>
	</div>	

</div>
<?php get_footer(); ?>