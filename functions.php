<?php
remove_action('template_redirect', 'wp_shortlink_header', 11);
add_filter('term_description', 'clear_term_description');
add_filter('login_errors', create_function('$a', "return null;"));
add_action('template_redirect', 'template_redirect_attachment');
function template_redirect_attachment()
{
  global $post;
  // Перенаправляем на основную запись:
  if (is_attachment()) {
    wp_redirect(get_permalink($post->post_parent), 301);
  }
}
function clear_term_description($value)
{
  return preg_replace(array('@<p>@', '@</p>@'), '', $value);
}
add_editor_style();
add_theme_support('post-thumbnails');
add_filter('excerpt_length', function () {
  return 55;
});
add_filter('excerpt_more', function ($more) {
  return '...';
});
// убираем мусор из шапки	
remove_action('wp_head', 'wp_generator');


//подключаем меню
function register_main_menus()
{
  register_nav_menus(
    array(
      'primary-menu' => 'Главное меню',
      'mobile-menu' => 'Мобильное меню',
      'uslugi-menu' => 'Блок услуг',
    )
  );
}
if (function_exists('register_nav_menus')) add_action('init', 'register_main_menus');

// подключаем хлебные крошки от dimox

function dimox_breadcrumbs()
{

  $text['home']     = 'Главная страница'; // текст ссылки "Главная"
  $text['category'] = '%s'; // текст для страницы рубрики
  $text['search']   = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
  $text['tag']      = 'Записи с меткой "%s"'; // текст для страницы тега
  $text['author']   = 'Статьи автора %s'; // текст для страницы автора
  $text['404']      = 'Ошибка 404'; // текст для страницы 404

  $show_current   = 1; // 1 - показывать название текущей статьи/страницы/рубрики, 0 - не показывать
  $show_on_home   = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
  $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
  $show_title     = 1; // 1 - показывать подсказку (title) для ссылок, 0 - не показывать
  $delimiter      = ' &raquo; '; // разделить между "крошками"
  $before         = ''; // тег перед текущей "крошкой"
  $after          = ''; // тег после текущей "крошки"
  /* === КОНЕЦ ОПЦИЙ === */

  global $post;
  $home_link    = home_url('/');
  $link_before  = '<span typeof="v:Breadcrumb">';
  $link_after   = '</span>';
  $link_attr    = ' rel="v:url" property="v:title"';
  $link         = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
  $parent_id    = $parent_id_2 = $post->post_parent;
  $frontpage_id = get_option('page_on_front');

  if (is_home() || is_front_page()) {

    if ($show_on_home == 1) echo '<span class="breadcrumbs"><a href="' . $home_link . '">' . $text['home'] . '</a></span>';
  } else {

    echo '<span class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">';
    if ($show_home_link == 1) {
      echo sprintf($link, $home_link, $text['home']);
      if ($frontpage_id == 0 || $parent_id != $frontpage_id) echo $delimiter;
    }

    if (is_category()) {
      $this_cat = get_category(get_query_var('cat'), false);
      if ($this_cat->parent != 0) {
        $cats = get_category_parents($this_cat->parent, TRUE, $delimiter);
        if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
        $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
        $cats = str_replace('</a>', '</a>' . $link_after, $cats);
        if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
        echo $cats;
      }
      if ($show_current == 1) echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
    } elseif (is_search()) {
      echo $before . sprintf($text['search'], get_search_query()) . $after;
    } elseif (is_day()) {
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
      echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F')) . $delimiter;
      echo $before . get_the_time('d') . $after;
    } elseif (is_month()) {
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
      echo $before . get_the_time('F') . $after;
    } elseif (is_year()) {
      echo $before . get_the_time('Y') . $after;
    } elseif (is_single() && !is_attachment()) {
      if (get_post_type() != 'post') {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
        if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category();
        $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $delimiter);
        if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
        $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
        $cats = str_replace('</a>', '</a>' . $link_after, $cats);
        if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
        echo $cats;
        if ($show_current == 1) echo $before . get_the_title() . $after;
      }
    } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
    } elseif (is_attachment()) {
      $parent = get_post($parent_id);
      $cat = get_the_category($parent->ID);
      $cat = $cat[0];
      $cats = get_category_parents($cat, TRUE, $delimiter);
      $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
      $cats = str_replace('</a>', '</a>' . $link_after, $cats);
      if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
      echo $cats;
      printf($link, get_permalink($parent), $parent->post_title);
      if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
    } elseif (is_page() && !$parent_id) {
      if ($show_current == 1) echo $before . get_the_title() . $after;
    } elseif (is_page() && $parent_id) {
      if ($parent_id != $frontpage_id) {
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          if ($parent_id != $frontpage_id) {
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
          }
          $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo $breadcrumbs[$i];
          if ($i != count($breadcrumbs) - 1) echo $delimiter;
        }
      }
      if ($show_current == 1) {
        if ($show_home_link == 1 || ($parent_id_2 != 0 && $parent_id_2 != $frontpage_id)) echo $delimiter;
        echo $before . get_the_title() . $after;
      }
    } elseif (is_tag()) {
      echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
    } elseif (is_author()) {
      global $author;
      $userdata = get_userdata($author);
      echo $before . sprintf($text['author'], $userdata->display_name) . $after;
    } elseif (is_404()) {
      echo $before . $text['404'] . $after;
    }

    if (get_query_var('paged')) {
      if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) echo ')';
    }

    echo '</span><!-- .breadcrumbs -->';
  }
} // end dimox_breadcrumbs()

/*  ---------------------------------------     */
class My_Walker_Nav_Menu extends Walker_Nav_Menu
{

  /**
   * Starts the element output.
   *
   * @since 3.0.0
   * @since 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
   *
   * @see Walker::start_el()
   *
   * @param string   $output Passed by reference. Used to append additional content.
   * @param WP_Post  $item   Menu item data object.
   * @param int      $depth  Depth of menu item. Used for padding.
   * @param stdClass $args   An object of wp_nav_menu() arguments.
   * @param int      $id     Current item ID.
   */
  public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {
    if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = ($depth) ? str_repeat($t, $depth) : '';

    $classes = empty($item->classes) ? array() : (array) $item->classes;
    $classes[] = 'menu-item-' . $item->ID;

    $args = apply_filters('nav_menu_item_args', $args, $item, $depth);

    $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth));
    $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

    $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth);
    $id = $id ? ' id="' . esc_attr($id) . '"' : '';

    // создаем HTML код элемента меню
    $output .= $indent . '<div class="col-xs-6 col-sm-6 col-md-3 text-center">';

    $atts = array();
    $atts['title']  = !empty($item->attr_title) ? $item->attr_title : '';
    $atts['target'] = !empty($item->target)     ? $item->target     : '';
    $atts['rel']    = !empty($item->xfn)        ? $item->xfn        : '';
    $atts['href']   = !empty($item->url)        ? $item->url        : '';

    $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);

    $attributes = '';
    foreach ($atts as $attr => $value) {
      if (!empty($value)) {
        $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }

    $title = apply_filters('the_title', $item->title, $item->ID);
    $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);

    $item_output = $args->before;
    $item_output .= '<a class="grid_banner"' . $attributes . '>';
    $item_output .= $args->link_before . $title . $args->link_after;
    $item_output .= '</a></div>';

    $item_output .= $args->after;

    $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
  }
}
/*  ---------------------------------------     */




include_once 'functions-shortcodes.php';  // Шорткоды


/*  ---------------------------------------     */
function get_include_contents($filename)
{
  if (is_file($filename)) {
    ob_start();
    include $filename;
    return ob_get_clean();
  }
  return false;
}
