
var phone_mask = "+9 (999) 999-99-99";
$('[type="tel"]').mask(phone_mask);


jQuery(document).ready(function ($) {

	/* мобильное меню */

	$("#mobilemenu").mmenu({
		position: "left",
		classes: "mm-slide",
		counters: true,
		header: {
			add: true,
			update: true,
			title: "Мобильное меню"
		}
	});

});



$(document).ready(function () {

	$.each($('.owl-reviews .owl-item'), function () {
		var src_path = $(this).find("img").data("full_src");
		$(this).data("mfp-src", src_path);
		$(this).attr("data-mfp-src", src_path);

	});

	$('.owl-reviews').magnificPopup({
		delegate: '.owl-item',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-with-zoom',
		removalDelay: 200,
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0, 1]
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function (item) { return item.el.attr('title'); }
		},
		callbacks: {
			beforeOpen: function () {
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-iframe');
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		}
	});

	setTimeout(() => $(".call-courier").addClass("animation-iteration-count"), 2000);
	setTimeout(() => $(".banner-discount").addClass("animation-banner-discount"), 1000);





	//
	//
	//
	jQuery(function () {
		var addressFrom = jQuery('[name="address_from"]');
		var addressTo = jQuery('[name="address_to"]');
		addressFrom.fias({
			oneString: true,
		});
		addressTo.fias({
			oneString: true,
		});
	});
});
