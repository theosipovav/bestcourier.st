<?php
/*
Template Name: Отчеты
Template Post Type: page
*/
?>
<?php	
$action = (isset($_POST['action'])) ? $_POST['action'] : "";		
$ordvl 	= (isset($_POST['ordvl'])) ? $_POST['ordvl'] : "";		
$mailvl = (isset($_POST['mailvl'])) ? $_POST['mailvl'] : "";

if($action == "exit"){
	setcookie( "visitps", "", time() + 10800  );
	$_COOKIE['visitps'] = null;
	$action = null;
	$ordvl = null;
	$mailvl = null;
}

if (!empty($_COOKIE['visitps'])){
	$str1 = $_COOKIE['visitps'];
	list ($loginbest, $passbest) = explode("/", $str1);	
} else {		
	$loginbest = (isset($_POST['loginbest'])) ? $_POST['loginbest'] : "";		
	$passbest = (isset($_POST['passbest'])) ? $_POST['passbest'] : "";		
}
	
$pr = 0;
if(!empty($loginbest) && !empty($passbest)){
	$f = fopen("clients/clients.txt", "r");
	while(!feof($f)) {
		$fl = iconv('windows-1251', 'utf-8', fgets($f));
		if (!empty($fl)){
			list ($code, $name, $pwdus) = explode("\t", $fl);
			$pwdus = Trim( $pwdus );	
			if( $code == $loginbest && $pwdus == $passbest && $loginbest!="" && $passbest!="" ){
				$pr=1;
				$company = $name;
				break;
			}
		}
	}
	fclose($f);	
	
	if($action == "setcookie" && $pr==1 ){
		//setcookie( "visitps", $visitps, time() + 24 * 3600 );
		$str[0] = $loginbest; 
		$str[1] = $passbest; 
		$str1 = join("/", $str );
		setcookie( "visitps", $str1, time() + 10800  );
	} 	
}

	
?>
<?php if( $pr == 0 || empty($ordvl)){ ?>
<?php get_header(); ?>
<div class="main_title">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php if (have_posts()) : 
						while (have_posts()) : the_post();  ?>
							<h1><?php the_title(); ?></h1>
					<?php endwhile; ?>
				<?php endif; ?>	
				<div id="dimox_breadcrumbs">
				<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
<?php } ?>

<?php if( $pr == 0 ){ ?>
	<div class="h4">Мониторинг заказа</div>
	
	<p>Дата последнего обновления данных: <?php echo date("d.m.Y H:i", filemtime("clients/mails.txt")); ?></p>
	
	<form action="/lichnyj-kabinet/" method="POST">	
		<input type="hidden" name="action" value="setcookie">
		
		<div class="form-group">
			<label>Введите логин:</label>
			<input type="text" name="loginbest" class="form-control" placeholder="Укажите Ваш логин">	
		</div>
		<div class="form-group">
			<label>Введите пароль:</label>
			<input type="password" name="passbest" class="form-control" placeholder="Укажите Ваш пароль">
		</div>	
		<div class="form-group">
			<button type="submit" class="btn btn-success">Посмотреть заказ</button>
		</div>		
	</form>

	


<?php } else if (empty($ordvl)){ ?>

		<p>Дата последнего обновления данных:<?php echo date("d.m.Y H:i", filemtime("clients/mails.txt")); ?></p>
		<p>Компания: <b><?php echo $company; ?></b></p>
		<p>Код: <b><?php echo $loginbest; ?></b></p>
		<br>
	

		<form action="/lichnyj-kabinet/" method="POST" id="exit_lk">
			<input type="hidden" name="action" value="exit">
		</form>
		<form action="/lichnyj-kabinet/" method="POST" target="_blank">
			<input type="hidden" name="action" value="setorder">
			<div class="form-group">
				<label>Выберите номер заказа:</label>
				<select name="ordvl" class="form-control">			
					<?php	
						$f = fopen("clients/orders.txt", "r");
						while(!feof($f)) {
						$fl = iconv('windows-1251', 'utf-8', fgets($f));	
						if (!empty($fl)) {
							list ($code, $order) = explode("\t", $fl);
							$order = Trim( $order );
							if( $code == $loginbest && $loginbest!="" ){
								if( $order == $ordvl ) echo "<option SELECTED>" . $order;	
								else echo "<option>" . $order;
							}	
						}
						}
			
						fclose($f);
					?>
				</select>
			</div>
			<div class="form-group">
				<label>Введите номер отправления (если необходимо):</label>
				<input name="mailvl" class="form-control">
			</div>
			<div class="form-group">	
				<p>Данные будут отображены в виде табличного отчёта в новом окне.</p>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">Посмотреть отчет</button>
				<button type="button" name="action_exit" value="exit" class="btn btn-default">Выход</button>		
			</div>
		</form>
		<script>
			$('[name="action_exit"]').on("click", function(){
				$('#exit_lk').submit();
			});
		</script>

<?php } ?> 

<?php if(!empty($ordvl)){ ?> 	
	<p>Заказ №<?php echo $ordvl; ?></p>
	<br />
	<table class="w" border=1 cellspacing=1 cellpadding=1>
		<tr>		
			<td>Номер отправления</td><!-- Номер заказа - 0 -->
			<td>№</td>
			<td>№ счета</td>
			<td>Компания</td>
			<td>Ф.И.О.</td>
			<td>Состояние</td><!-- Реальное состояние - 6 -->
			<td>Количество посещений</td><!-- Реальное количество посещений - 8 -->	
			<td>Дата</td><!-- Реальная дата - 10 -->
			<td>Роспись</td>
			<td>Квитанция</td>
			<td>Адрес</td>
			<td>Зона</td>
			<td>Причина недоставки</td>
			<td>Прозвон</td>
			<td>Телефон</td>
		</tr>


		<?php 
			$f = fopen("clients/mails.txt", "r");
			while(!feof($f)){ 	    

				$fl = iconv('windows-1251', 'utf-8', fgets($f));		
		
				$str = explode("\t", $fl);		
		
				if($str[0] == $ordvl && ($mailvl == "" || $mailvl == $str[1])){
		?>	
			<tr>
				<td><?php echo $str[1]; ?></td>
				<td><?php echo $str[2]; ?></td>
				<td><?php echo $str[3]; ?></td>
				<td><?php echo $str[4]; ?></td>
				<td><?php echo $str[5]; ?></td>
				<td><?php echo $str[7]; ?></td>
				<td><?php echo $str[9]; ?></td>
				<td><?php echo $str[11]; ?></td>
				<td><?php echo $str[12]; ?></td>
				<td>
			
					<?php
						$rpdf = Trim($str[19]);
						while(strlen($rpdf)<5) {
							$rpdf = '0'.$rpdf;	                  
						}
						$rpdf = Trim($ordvl).$rpdf;
						while(strlen($rpdf)<11) {
							$rpdf = '0'.$rpdf;	                  
						}
			
						$somePath = 'slip/'.$rpdf.'*.pdf';
			
						$nfp = 1;
						$prob = '';
			
						foreach (glob("$somePath") as $docFile) { 			  
			
						if ( $nfp>1 ) {
							echo '<br>'; 
							$prob = ' ('.$nfp.')';	
						}
					?>
					<a href="../<?php echo $docFile; ?>"><?php echo '№ '.Trim( $str[19] ).$prob; ?></a>
					<?php                                                               
						$nfp = $nfp + 1;
						}

						$rpdf = $rpdf.'.pdf';
					?>
			
				</td>
				<td><?php echo $str[13]; ?></td>
				<td><?php echo $str[14]; ?></td>
				<td><?php echo $str[15]; ?></td>
				<td><?php echo $str[16]; ?></td>
				<td><?php echo $str[17]; ?></td>
			</tr>
		<?php
			}	

		}
		fclose($f);
	}
?>
	</table>
</div>

<?php if( $pr == 0 || empty($ordvl)){ ?>
	</div></div></div>
	<?php get_footer(); ?>
<?php } ?>