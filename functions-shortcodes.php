<?php
// update 24-10-2020

/** 
 * Форма для расчета стоимости
 * [calculation_cost]
 * 
 */
function shortcodeCalculationCost($atts = [], $content = null)
{
    $out = get_include_contents(locate_template('/shortcode-template/calculation-cost.php'));
    return $out;
}
add_shortcode('calculation_cost', 'shortcodeCalculationCost');
