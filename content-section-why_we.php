<?php
// Секция "ТРИ ПРИЧИНЫ ВЫБРAТЬ BESTCOURIER ТРИ ПРИЧИНЫ ВЫБРAТЬ BESTC"
?>
<div class="grid-row-5dc017173e354088498157 ">
    <div class="container">
        <div class="g_row">
            <div class="grid_col-5dc017173eb85078094938">
                <div class="grid_col_wrp">
                    <div class="html_56 three_reasons m_title_10" data-uniqid="m_html--5dd538f5177df295492733" data-version="v.0002">
                        <div class="title_wrap_top">
                            <div class="title">Три причины <span>выбрaть BestCourier</span></div>
                            <div class="post_title">Три причины <span>выбрaть BestCourier</span></div>
                        </div>
                        <div class="cont_module_wrp">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 text-center">
                                    <div class="ico"><img src="<?php bloginfo('template_directory'); ?>/images/ico-best/1.png"></div>
                                    <div class="title">Стабильная компания</div>
                                </div>
                                <div class="col-xs-12 col-sm-4 text-center">
                                    <div class="ico"><img src="<?php bloginfo('template_directory'); ?>/images/ico-best/2.png"></div>
                                    <div class="title">Клиентоориентированность</div>
                                </div>
                                <div class="col-xs-12 col-sm-4 text-center">
                                    <div class="ico"><img src="<?php bloginfo('template_directory'); ?>/images/ico-best/3.png"></div>
                                    <div class="title">Лучшие тарифы</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>