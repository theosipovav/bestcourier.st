<?php
$table["6d"] = get_field("6d", "12");
$table["4d"] = get_field("4d", "12");
$table["3d"] = get_field("3d", "12");
$table["1d"] = get_field("1d", "12");
$table["1d_12"] = get_field("1d_12", "12");
$table["2h"] = get_field("2h", "12");
$table["4h"] = get_field("4h", "12");

$tarifi = array();


foreach ($table as $k => $v) {
	foreach ($v["body"] as $p) {
		$tarifi[$k][$p[0]["c"]] = $p[1]["c"];
	}
}
?>
<script>
	$(document).ready(function() {
		var tarifi = <?php echo json_encode($tarifi); ?>,
			weight_more = 50,

			count_send_inp = $('[name="count_send"]'),
			weight_inp = $('[name="weight"]'),
			period_inp = $('[name="period"]'),
			take_inp = $('[name="take"]'),

			if_zabor = $('#if_zabor'),
			if_error = $('#if_error'),
			calc_info_if_day = $('#if_day'),
			calc_info_if_hour = $('#if_hour'),
			calc_result = $('#calc_result'),
			courier_order = $('#courier_order'),
			calc_btn_calculate = $('#calc_btn_calculate');

		calc_btn_calculate.on("click", function() {
			if (calc_valid()) {
				if_error.slideUp();
				get_result();
			} else {
				if_error.slideDown();
				calc_info_if_day.slideUp();
				calc_info_if_hour.slideUp();
				calc_result.slideUp();
				courier_order.slideUp();
			}
		});

		period_inp.on("change", function() {
			var period_val = $(period_inp).val();

			if (period_val != "") {
				if ($.inArray(period_val, ["4h", "2h", "1d_12"]) > -1) {
					if_zabor.slideUp();
				} else {
					if_zabor.slideDown();
				}
			} else {
				if_zabor.slideUp();
			}
		});

		/*
		count_send_inp.on("keyup", function(){get_result();});
		weight_inp.on("keyup", function(){get_result();});
		period_inp.on("change", function(){get_result();});
		take_inp.on("change", function(){get_result();});
		*/

		function calc_valid() {
			var count_send_val = Number.parseInt($(count_send_inp).val());
			var weight_val = Number.parseInt($(weight_inp).val());
			var period_val = $(period_inp).val();
			var error = 0;



			if ((!count_send_val || count_send_val <= 0) || (!weight_val || weight_val <= 0) || period_val == '') {
				error = 1;
			}
			return !error;
		}

		function get_result() {
			var count_send_val = Number.parseInt($(count_send_inp).val());
			var weight_val = Number.parseInt($(weight_inp).val());
			var period_val = $(period_inp).val();
			var take_val = 0;

			if (period_val != "") {
				if ($.inArray(period_val, ["4h", "2h", "1d_12"]) > -1) {
					//if_zabor.slideUp();
					calc_info_if_day.slideUp();
					calc_info_if_hour.slideDown();
				} else {
					take_val = $(take_inp).prop("checked");
					//if_zabor.slideDown();
					calc_info_if_day.slideDown();
					calc_info_if_hour.slideUp();
				}
			} else {
				//if_zabor.slideUp();
				calc_info_if_day.slideUp();
				calc_info_if_hour.slideUp();
			}

			var price_zabor = 0;
			console.log('take_val: ' + take_val);
			console.log('count_send_val: ' + count_send_val);
			console.log('weight_val:' + weight_val);

			if (take_val) {
				var all_weight = count_send_val * weight_val;
				console.log(all_weight);
				if (all_weight < 5000) {
					price_zabor = 350;
				} else {
					price_zabor = 850;
					price_zabor = 350;
				}
			}
			console.log('price_zabor: ' + price_zabor);

			var calc_tarif = 0;

			if (weight_val > 0) {
				$.each(tarifi[period_val], function(w, price) {
					if (weight_val <= w) {
						calc_tarif = price;
						return false;
					} else {
						calc_tarif = Number.parseInt(price);
						calc_tarif += Math.ceil((weight_val - Number.parseInt(w)) / 500) * weight_more;

						console.log(Math.ceil((weight_val - Number.parseInt(w)) / 500));


						/*
						console.log("price", Number.parseInt(price));
						console.log("weight_val", weight_val);
						console.log("w", w);					
						console.log("weight_more", weight_more);					
						console.log("post", (weight_val - w) / 500 * weight_more);	
						*/
					}
				});
			}



			var data_result = Number.parseInt(count_send_val * calc_tarif) + Number.parseInt(price_zabor);
			calc_result.slideDown();
			courier_order.slideDown();

			calc_result.html("~ " + data_result + " руб.");
		}
	});
</script>
<style>
	.calc_text_danger {
		margin: 0px 0px 15px;
		padding: 20px;
		border-left: 3px solid #eee;
		background-color: #ffe2e2;
		border-color: #f11010;
	}

	.calc_text_info {
		padding: 20px;
		border-left: 3px solid #eee;
		background-color: #f4f8fa;
		border-color: #bce8f1;
	}

	.calc_text_info p:last-child {
		margin-bottom: 0px
	}

	.if_zabor {
		display: none;
	}

	.d-none {
		display: none;
	}

	.mg_y_20 {
		margin-top: 20px;
		margin-bottom: 20px
	}

	.mg_b_20 {
		margin-bottom: 20px
	}

	.mg_0 {
		margin: 0px
	}
</style>