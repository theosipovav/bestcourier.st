<?php
// Секция "Популярные услуги"
?>
<div class="grid-section--5dd518bfc3127074564568 ">
    <div class="container">
        <div class="g_row">
            <div class="grid_col-5dd518bfc3d70c29274175">
                <div class="grid_col_wrp">
                    <div class="html_53 section_fast_link " data-uniqid="m_html--5dd5176a37dfe251917802" data-version="v.0002">
                        <div class="title_wrap_top"> </div>
                        <div class="cont_module_wrp">
                            <div class="row">
                                <div class="col-sm-6 col-md-3 ">
                                    <a class="grid_banner" href="/raschet-stoimosti/"> <span class="image grid_banner--raschet_stoimosti"></span> <span class="caption">Расчет стоимости</span> </a>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <a class="grid_banner" href="/tarify/"> <span class="image grid_banner--tarify"></span> <span class="caption">Тарифы</span> </a>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <a class="grid_banner" href="/dokumenty/"> <span class="image grid_banner--zakluchenie_dogovora"></span> <span class="caption">Заключить договор</span> </a>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <a class="grid_banner" href="/lichnyj-kabinet/"> <span class="image grid_banner--monitorig_zakaza"></span> <span class="caption">Мониторинг заказа</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>